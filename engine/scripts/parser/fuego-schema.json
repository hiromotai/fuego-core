{
    "$schema":"http://json-schema.org/schema#",
    "id":"http://www.fuegotest.org/download/fuego_schema_v1.0.json",
    "title":"test_suite",
    "description":"A test suite JSON object corresponding to a single Fuego job run or execution",
    "definitions":{
        "status":{
            "type":"string",
            "description":"The status of the execution of this test case",
            "enum":[
                "PASS",
                "FAIL",
                "SKIP"
            ]
        },
        "measurement":{
            "title":"measurement",
            "description":"A measurement registered by a test case",
            "type":"object",
            "properties":{
                "name":{
                    "type":"string",
                    "description":"The name given to this measurement"
                },
                "unit":{
                    "type":"string",
                    "description":"The unit of this measurement"
                },
                "measure":{
                    "type":[
                        "string",
                        "number",
                        "integer"
                    ],
                    "description":"The data measured during the test case execution; the value will be interpreted based on the $unit field"
                },
                "status":{
                    "$ref":"#/definitions/status"
                }
            },
            "required":[
                "name",
                "measure"
            ]
        },
        "attachment":{
            "title":"attachment",
            "description":"the location of a file",
            "type":"object",
            "properties":{
                "name":{
                    "type":"string",
                    "description":"The name given to the attachment"
                },
                "server_uri":{
                    "type":"string",
                    "description":"A URI that points to this attachment"
                },
                "path":{
                    "type":"string",
                    "description":"A path in the test results package that identifies this attachment"
                }
            },
            "required":[
                "path"
            ]
        },
        "test_case":{
            "type":"object",
            "description":"A test case is the smallest test program",
            "properties":{
                "name":{
                    "type":"string",
                    "description":"The name given to this test case"
                },
                "status":{
                    "$ref":"#/definitions/status"
                },
                "duration_ms":{
                    "type":"number",
                    "description":"The number of ms it took to execute this test case"
                },
                "measurements":{
                    "type":"array",
                    "description":"Array of measurement objects registered by this test case",
                    "items":{
                        "$ref":"#/definitions/measurement"
                    }
                },
                "attachments":{
                    "type":"array",
                    "description":"List of attachment objects produced by this test case",
                    "items":{
                        "$ref":"#/definitions/attachment"
                    },
                    "additionalItems":true
                }
            },
            "required":[
                "name",
                "status"
            ]
        },
        "test_set":{
            "title":"test_set",
            "description":"A test set is a group of test cases",
            "type":"object",
            "properties":{
                "name":{
                    "type":"string",
                    "description":"The name given to this test set"
                },
                "status":{
                    "$ref":"#/definitions/status"
                },
                "duration_ms":{
                    "type":"number",
                    "description":"The number of ms it took to execute the test set"
                },
                "test_cases":{
                    "type":"array",
                    "description":"The list of test case objects executed by this test set",
                    "items":{
                        "$ref":"#/definitions/test_case"
                    }
                }
            },
            "required":[
                "name"
            ]
        }
    },
    "type":"object",
    "properties":{
        "schema_version":{
            "type":"string",
            "description":"The version number of this JSON schema",
            "enum":[
                "1.0"
            ]
        },
        "name":{
            "type":"string",
            "description":"The name given to this test suite"
        },
        "status":{
            "$ref":"#/definitions/status"
        },
        "duration_ms":{
            "type":"number",
            "description":"The number of ms it took to execute the entire test suite"
        },
        "test_sets":{
            "type":"array",
            "description":"The list of test set objects, executed by this test suite",
            "items":{
                "$ref":"#/definitions/test_set"
            }
        },
        "metadata":{
            "type":"object",
            "description":"Object to store Fuego parameters and other accessory data",
            "properties":{
                "fuego_version":{
                    "type":"string",
                    "description":"The version of fuego (e.g. v1.1-9234a234)"
                },
                "fuego_core_version":{
                    "type":"string",
                    "description":"The version of fuego core (e.g. v1.1-234544ac)"
                },
                "testsuite_version":{
                    "type":"string",
                    "description":"string that identifies the testsuite source code (e.g. repo:commit_id, tarball version..)"
                },
                "host_name":{
                    "type":"string",
                    "description":"The name of the host executing this test suite"
                },
                "board":{
                    "type":"string",
                    "description":"The name of the board"
                },
                "board_instance":{
                    "type":"string",
                    "description":"The instance identifier of the board (for LAVA)"
                },
                "compiled_on":{
                    "type":"string",
                    "description":"The machine where the test was compiled"
                },
                "job_name":{
                    "type":"string",
                    "description":"The name of the job"
                },
                "kernel_version":{
                    "type":"string",
                    "description":"The version of the kernel running on the board"
                },
                "toolchain":{
                    "type":"string",
                    "description":"The toolchain used to build the test"
                },
                "start_time":{
                    "type":"string",
                    "description":"timestamp (e.g.: 2017-06-29_17:44:25-0700) when the test suite execution was started"
                },
                "test_plan":{
                    "type":"string",
                    "description":"The test plan used"
                },
                "test_spec":{
                    "type":"string",
                    "description":"The test spec used"
                },
                "build_number":{
                    "type":"number",
                    "description":"The build number"
                },
                "keep_log":{
                    "type":"boolean",
                    "description":"The keep_log flag"
                },
                "reboot":{
                    "type":"boolean",
                    "description":"The reboot flag"
                },
                "rebuild":{
                    "type":"boolean",
                    "description":"The rebuild flag"
                },
                "target_precleanup":{
                    "type":"boolean",
                    "description":"The target_precleanup flag"
                },
                "target_postcleanup":{
                    "type":"boolean",
                    "description":"The target_postcleanup flag"
                },
                "workspace":{
                    "type":"string",
                    "description":"The workspace path"
                },
                "attachments":{
                    "type":"array",
                    "description":"List of attachment objects produced by this test suite",
                    "items":{
                        "$ref":"#/definitions/attachment"
                    },
                    "additionalItems":true
                }
            },
            "required":[
                "fuego_version",
                "host_name",
                "board_type",
                "job_name",
                "test_spec",
                "start_time"
            ]
        }
    },
    "required":[
        "schema_version",
        "name",
        "status",
        "metadata"
    ]
}
