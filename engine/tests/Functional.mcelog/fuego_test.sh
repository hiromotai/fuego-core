function test_pre_check {
    is_on_target_path mcelog PROGRAM_MCELOG
    assert_define PROGRAM_MCELOG "Missing 'mcelog' program on target board"
}

function test_deploy {
    put $TEST_HOME/mcelog_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/
    put -r $TEST_HOME/tests $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR;\
    ./mcelog_test.sh"
}

function test_processing {
    log_compare "$TESTDIR" "0" "TEST-FAIL" "n"
}
