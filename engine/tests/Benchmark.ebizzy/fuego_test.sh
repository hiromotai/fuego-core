tarball=ebizzy-0.3.tar.gz

function test_build {
    $CC -Wall -Wshadow -lpthread  -o ebizzy ebizzy.c
}

function test_deploy {
	put ebizzy  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
        assert_define BENCHMARK_EBIZZY_CHUNKS
        assert_define BENCHMARK_EBIZZY_CHUNK_SIZE
        assert_define BENCHMARK_EBIZZY_TIME
        assert_define BENCHMARK_EBIZZY_THREADS

	report "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./ebizzy -m -n $BENCHMARK_EBIZZY_CHUNKS -P -R -s $BENCHMARK_EBIZZY_CHUNK_SIZE  -S $BENCHMARK_EBIZZY_TIME -t $BENCHMARK_EBIZZY_THREADS"  
}


