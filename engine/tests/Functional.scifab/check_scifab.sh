#!/bin/sh
# PDX-License-Identifier: MIT
#
# check_scifab.sh - checks for the presence of sci information
#   in dmesg and /proc/interrupts
#
# Usage: check_scifab.sh <sci_name> [<pattern> <count>]
#
# Output is in TAP13 format
#
# Author: Qiu Tingting <Qiutt (at) cn.fujitsu.com>
#

usage() {
    cat <<HERE
Usage: check_scifab.sh [-h] <sci_name> [<pattern> <count>]

Arguments:
 -h  = show this usage help

sci_name is the partial name of the interrupt in /proc/interrupts
  (eg:""sci\|serial")
pattern is the search pattern used for matching the initialization
  information of the serial device in dmesg output(eg:".*ttySC.*")
count is the matching count of pattern
HERE
   exit 0
}

# parse arguments
IRQ_NAME="$1"

if [ -z "$IRQ_NAME" -o "$IRQ_NAME" = "-h" ] ; then
    usage
fi

echo "TAP version 13"

echo "Test for sci attributes"
echo "IRQ_NAME=\"$IRQ_NAME\""

tap_id=1
desc="${tap_id} Check interrupt name in proc filesystem"
#search partial name of sci in /proc/interrupts
if grep "$IRQ_NAME" /proc/interrupts ; then
    echo "ok ${desc}"
else
    echo "not ok ${desc}"
    echo "  Did not find $IRQ_NAME in /proc/interrupts"
fi

SEARCH_PATTERN="$2"
SEARCH_COUNT="$3"

echo "SEARCH_PATTERN=\"$SEARCH_PATTERN\""
echo "SEARCH_COUNT=\"$SEARCH_COUNT\""

if [ -n "$SEARCH_PATTERN" -a -n "$SEARCH_COUNT" ] ; then
    tap_id=$(( $tap_id + 1 ))
    desc="${tap_id} Check sci initialization string in dmesg"
    #calculate the sci initialization string count in dmesg output
    matching_count=`dmesg | grep "$SEARCH_PATTERN" | wc -l`
    if [ $matching_count -eq $SEARCH_COUNT ] ; then
        echo "ok ${desc}"
    else
        echo "not ok ${desc}"
        echo "  the matching count for '$SEARCH_PATTERN' is not as expected,expected count is '$SEARCH_COUNT',reality is '$matching_count'"
    fi
else
    echo "Pattern or count is not defined. This case is skipped."
fi

echo "1..$tap_id"
