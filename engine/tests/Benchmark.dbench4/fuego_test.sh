# gitrepo=https://github.com/sahlberg/dbench.git
tarball=dbench-4.00.tar.gz

function test_pre_check {
    assert_define BENCHMARK_DBENCH_MOUNT_BLOCKDEV
    assert_define BENCHMARK_DBENCH_MOUNT_POINT
    assert_define BENCHMARK_DBENCH_TIMELIMIT
    assert_define BENCHMARK_DBENCH_NPROCS

    # check if dbench 4+ exists on the target
    is_on_target_path dbench PROGRAM_DBENCH
    if [ ! -z "$PROGRAM_DBENCH" ]; then
        help=$(cmd "dbench --help") || true
        version=$(echo $help | head -1 | cut -d' ' -f 3)
        if version_lt $version 4.00 ; then
            echo "dbench found on the target but version $version is not supported."
            echo "Using Fuego's version of dbench."
            PROGRAM_DBENCH=""
        else
            echo "dbench found on the target (version $version)."
        fi
    fi

    # check if the loadfile exists on the target
    LOADFILE=${BENCHMARK_DBENCH_LOADFILE:-client.txt}
    LOADFILE_ONTARGET=""
    if is_abs_path $LOADFILE; then
        if $(cmd "test -e $LOADFILE"); then
            LOADFILE_ONTARGET=$LOADFILE
            echo "$LOADFILE_ONTARGET found on the target."
        else
            echo "Warning: $LOADFILE not found on the target, using Fuego's default."
            LOADFILE_ONTARGET=""
            LOADFILE="client.txt"
        fi
    else
        # check if the file is on the target board
        if $(cmd "test -e /usr/share/dbench/$LOADFILE"); then
            LOADFILE_ONTARGET="/usr/share/dbench/$LOADFILE"
            echo "$LOADFILE_ONTARGET found on the target."
        fi
    fi
}

function test_build {
    if [ -z "$PROGRAM_DBENCH" ]; then
        # prepare zlib first
        tar xvf $TEST_HOME/zlib-1.2.11.tar.gz
        cd zlib-1.2.11/
        ./configure --static
        make
        # compile dbench using libz.a
        cd ..
        ./autogen.sh
        ./configure --host=$HOST
        cp ./zlib-1.2.11/zconf.h .
        LDFLAGS="-static -L./zlib-1.2.11/" make
    else
        echo "Skipping build phase, dbench is already on the target"
    fi
}

function test_deploy {
    if [ -z "$PROGRAM_DBENCH" ]; then
        echo "Deploying dbench."
        put dbench $BOARD_TESTDIR/fuego.$TESTDIR/
        PROGRAM_DBENCH="$BOARD_TESTDIR/fuego.$TESTDIR/dbench"
    fi

    if [ -z "$LOADFILE_ONTARGET" ]; then
        echo "Deploying loadfiles/$LOADFILE."
        put loadfiles/$LOADFILE $BOARD_TESTDIR/fuego.$TESTDIR/
        LOADFILE_ONTARGET="$BOARD_TESTDIR/fuego.$TESTDIR/$LOADFILE"
    fi
}

function test_run {
    # check if the backend needs to be specified
    HELP_BACKEND=$(cmd "$PROGRAM_DBENCH --help" | grep backend) || true
    if [ -n "$HELP_BACKEND" ]; then
        echo "This dbench supports backends"
        if [ -z "$BENCHMARK_DBENCH_BACKEND" ]; then
            echo "Warning: backend not specified, trying to guess one."
            case $(basename $LOADFILE) in
                client.txt) BENCHMARK_DBENCH_BACKEND="fileio" ;;
                iscsi*) BENCHMARK_DBENCH_BACKEND="iscsi" ;;
                scsi.txt) BENCHMARK_DBENCH_BACKEND="scsi" ;;
                smb*) BENCHMARK_DBENCH_BACKEND="smb" ;;
                nfs*) BENCHMARK_DBENCH_BACKEND="nfs" ;;
                *) BENCHMARK_DBENCH_BACKEND="sockio" ;;
            esac
        fi
        echo "Using backend $BENCHMARK_DBENCH_BACKEND"
        BACKEND="-B $BENCHMARK_DBENCH_BACKEND"
    else
        echo "This dbench does not support backends"
        BACKEND=""
    fi

    hd_test_mount_prepare $BENCHMARK_DBENCH_MOUNT_BLOCKDEV \
        $BENCHMARK_DBENCH_MOUNT_POINT
    # TODO: timelimit does not seem to work like a timeout as expected
    report "$PROGRAM_DBENCH \
            -t $BENCHMARK_DBENCH_TIMELIMIT \
            $BACKEND -c $LOADFILE_ONTARGET \
            -D $BENCHMARK_DBENCH_MOUNT_POINT/fuego.$TESTDIR \
            $BENCHMARK_DBENCH_EXTRAPARAMS \
            $BENCHMARK_DBENCH_NPROCS; \
            sync"
    hd_test_clean_umount $BENCHMARK_DBENCH_MOUNT_BLOCKDEV \
        $BENCHMARK_DBENCH_MOUNT_POINT
}
