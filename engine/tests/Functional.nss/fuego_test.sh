function test_pre_check {
    assert_has_program shlibsign
}

function test_run {
    test="nss"
    report "shlibsign --help &> shlibsign-usage.txt
            if grep ".*Usage.*" shlibsign-usage.txt
            then
                echo ' -> $test: TEST-PASS'
            else
                echo ' -> $test: TEST-FAIL'
            fi"
}

function test_processing {
    log_compare "$TESTDIR" "0" "TEST-FAIL" "n"
}
