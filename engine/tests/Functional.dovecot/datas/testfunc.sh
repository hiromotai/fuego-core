# $1: Used to determine whether the command line execution result is correct or not
function exec_service_on_target {
local service_name="$1"
local action="$2"
if [ "$init_manager" = "sysvinit" ]
then 
    $service_name $action
elif [ "$init_manager" = "systemd" ]
then
    systemctl $action $service_name
fi
return
}
