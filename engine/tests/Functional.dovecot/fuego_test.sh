function test_pre_check {
    is_on_target_path dovecot PROGRAM_DOVECOT
    assert_define PROGRAM_DOVECOT "Missing 'dovecot' program on target board"
}

function test_deploy {
    put $FUEGO_CORE/engine/scripts/fuego_board_function_lib.sh $BOARD_TESTDIR/fuego.$TESTDIR
    put $TEST_HOME/dovecot_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/
    put -r $TEST_HOME/tests $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR;\
        sh -v dovecot_test.sh"
}

function test_processing {
    log_compare "$TESTDIR" "0" "TEST-FAIL" "n"
}
