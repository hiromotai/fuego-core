#!/bin/sh

#  In the target to start dovecot dovecot, to confirm the acquisition of the log.
#  check the keyword "LISTEN".

test="listen"

. "./fuego_board_function_lib.sh"

set_init_manager

exec_service_on_target dovecot stop

if exec_service_on_target dovecot start
then
    echo " -> start of dovecot succeeded."
else
    echo " -> start of dovecot failed."
    echo " -> $test: TEST-FAIL"
fi

if netstat -a --tcp | grep LISTEN
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

exec_service_on_target dovecot stop
