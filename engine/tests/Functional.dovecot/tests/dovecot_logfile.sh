#!/bin/sh

#  In the target to start dovecot dovecot, to confirm the acquisition of the log.
#  check the keyword "dovecot".


test="logfile"
service_name=""

. "./fuego_board_function_lib.sh"

set_init_manager

exec_service_on_target dovecot stop
rm -f /var/log/mail.log

exec_service_on_target $service_name restart

if exec_service_on_target dovecot start
then
    echo " -> start of dovecot succeeded."
else
    echo " -> start of dovecot failed."
    echo " -> $test: TEST-FAIL"
fi

if tail /var/log/mail.log | grep "dovecot"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

exec_service_on_target dovecot stop
