#!/usr/bin/env python3
import argparse
import requests
import sys
from io import BytesIO

import selenium.common.exceptions as selenium_exceptions
from selenium import webdriver
from selenium.webdriver.common.by import By
from PIL import Image


def public_attributes(obj):
    return [attr for attr in dir(By) if not attr.startswith('__')]


AVAILABLE_LOCATORS = public_attributes(By)
DEFAULT_VIEWPORT_RESOLUTION = "1920x1080"


class SeleniumSession:
    def __init__(self, resolution):
        options = webdriver.ChromeOptions()
        options.add_argument('headless')
        options.add_argument('no-sandbox')
        options.add_argument('window-size=%s' % resolution)
        options.add_experimental_option(
            'prefs', {'intl.accept_languages': 'en,en_US'})

        self.driver = webdriver.Chrome(chrome_options=options)
        self.driver.implicitly_wait(3)

    def __del__(self):
        if self.driver:
            self.driver.quit()

    def go(self, url):
        try:
            r = requests.get(url)
        except (requests.exceptions.MissingSchema,
                requests.exceptions.ConnectionError,
                requests.exceptions.ConnectTimeout) as e:
            print(e)
            return 404

        if r.status_code != requests.codes.ok:
            print("Unable to navigate to the page %s" % url)
            print("Please provide a valid URL")
            return r.status_code

        self.driver.get(url)
        return r.status_code

    def take_screenshot(self, locator_str, pattern):
        def take_element_screenshot(locator, pattern):
            try:
                element = self.driver.find_element(locator, pattern)
            except selenium_exceptions.NoSuchElementException:
                print("Element not found")
                return None

            location = element.location_once_scrolled_into_view
            size = element.size

            viewport_screenshot = Image.open(
                BytesIO(self.driver.get_screenshot_as_png()))
            element_screenshot = viewport_screenshot.\
                crop((location['x'], location['y'],
                      location['x'] + size['width'],
                      location['y'] + size['height'],))

            return element_screenshot

        def take_full_screenshot():
            return Image.open(
                BytesIO(self.driver.get_screenshot_as_png()))

        if not locator_str or not pattern:
            print("Taking full screenshot (locator and/or pattern are empty)")
            return take_full_screenshot()

        locator = getattr(By, locator_str, None)
        if not locator:
            print("Locator '%s' is not supported" % locator_str)
            return None

        print("Taking element screenshot")
        return take_element_screenshot(locator, pattern)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('url', help="""The URL from where a screenshot will be
                        taken""", type=str)
    parser.add_argument('-l', '--locator', help="the "
                        """locator that will be used to find the element on the
                        page. Available locators are: %s""" %
                        AVAILABLE_LOCATORS, default=None, type=str)
    parser.add_argument('-p', '--pattern', help="""The pattern that will be
                        used for locating the element.""",
                        default=None, type=str)
    parser.add_argument('-o', '--out', help="""Resulting screenshot filename.
                        Defaults to '${locator}_${pattern}.png' if those
                        arguments are given or 'full_viewport.png' if not.""",
                        default=None, type=int)
    parser.add_argument('-r', '--resolution', help="""Viewport resolution.
                        Defaults to %s. Make sure your page or element fits
                        this resolution, otherwise the results might be
                        cropped.""" % DEFAULT_VIEWPORT_RESOLUTION,
                        default=DEFAULT_VIEWPORT_RESOLUTION, type=str)
    args = parser.parse_args()

    def generate_out_filename():
        if not args.locator or not args.pattern:
            return 'full_screenshot.png'
        else:
            return args.locator + '_' + args.pattern + '.png'

    if not args.out:
        args.out = generate_out_filename()

    selenium = SeleniumSession(args.resolution)

    status_code = selenium.go(args.url)
    if status_code != requests.codes.ok:
        print("Unable to navigate to URL")
        return 1

    screenshot = selenium.take_screenshot(args.locator, args.pattern)
    if not screenshot:
        print("Unable to take a screenshot")
        return 1

    try:
        screenshot.save(args.out, format='PNG')
    except FileNotFoundError as e:
        print("Unable to save the screenshot to '%s'" % args.out)
        print("  Reason: %s" % e)
        return 1

    return 0


if __name__ == '__main__':
    sys.exit(main())
