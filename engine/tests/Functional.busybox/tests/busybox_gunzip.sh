#!/bin/sh

#  The testscript checks the following options of the command gunzip
#  1) Option: -c -t

test="gunzip"

mkdir test_dir
echo "This is a file to test gunzip." > ./test_dir/test1
gzip ./test_dir/test1
if [ "$(ls test_dir)" = "test1.gz" ]
then
    echo " -> $test: File test1.gz created."
else
    echo " -> $test: File test1.gz creation failed."
    echo " -> $test: TEST-FAIL"
    rm -rf test_dir
    exit
fi

if [ "$(busybox gunzip -c ./test_dir/test1.gz)" = "This is a file to test gunzip." ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
rm -rf test_dir
