#!/bin/sh

#  The testscript checks the following options of the command ping
#  1) Option none

test="ping"

euid=$(id -u)
if [ "$euid" = 0 ] ; then
    SUDO=""
else
    SUDO="sudo"
fi
$SUDO ping -c 3 $test_ipaddr > log

echo "log="
cat log

if grep "PING" log && grep "seq=1" log && grep "seq=2" log
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm log;
