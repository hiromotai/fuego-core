#!/bin/sh

#  The testscript checks the following options of the command openvt
#  1) Option none

test="openvt"

killall sleep
busybox openvt -f -c 2 sleep 15732
if  ps aux | grep "[s]leep 15732"
then
    echo " -> $test: ps aux | grep "[s]leep 15732" executed."
else
    echo " -> $test: ps aux | grep "[s]leep 15732" failed."
    echo " -> $test: TEST-FAIL"
    exit
fi;

kill $(pgrep -f "[s]leep 15732")
if ps aux | grep "[s]leep 15732"
then
    echo " -> $test: TEST-FAIL"
else
    echo " -> $test: TEST-PASS"
fi;
