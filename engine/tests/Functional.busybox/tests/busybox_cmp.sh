#!/bin/sh

#  The testscript checks the following options of the command cmp
#  1) Option: -l -s

test="cmp"

mkdir test_dir
echo "This is test file 1." > ./test_dir/test1
echo "This is test file 2." > ./test_dir/test2
if [ "$(busybox cmp test_dir/test1 test_dir/test2)" = "test_dir/test1 test_dir/test2 differ: char 19, line 1" ]
then
    echo " -> $test: command cmp succeeded."
else
    echo " -> $test: command cmp failed."
    echo " -> $test: TEST-FAIL"
    rm -rf test_dir
    exit
fi



if [ "$(busybox cmp -l test_dir/test1 test_dir/test2)" = "19  61  62" ]
then
    echo " -> $test: command cmp -l succeeded."
else
    echo " -> $test: command cmp -l failed."
    echo " -> $test: TEST-FAIL"
    rm -rf test_dir
    exit
fi

if [ "$(busybox cmp -s test_dir/test1 test_dir/test2)" = "" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -rf test_dir
