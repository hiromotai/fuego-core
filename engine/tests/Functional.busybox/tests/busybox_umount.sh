#!/bin/sh

#  The testscript checks the following options of the command umount
#  1) Option none

test="umount"

mkdir test_dir
mount -t tmpfs none test_dir

if mount | grep "none on .*test_dir"
then
    echo " -> $test: mount succeeded."
else
    echo " -> $test: mount failed."
    echo " -> $test: TEST-FAIL"
    exit
fi;

busybox umount test_dir
if mount | grep "none on .*test_dir"
then
    echo " -> $test: TEST-FAIL"
else
    echo " -> $test: TEST-PASS"
fi;
rmdir test_dir
