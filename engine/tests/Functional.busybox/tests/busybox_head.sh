#!/bin/sh

#  The testscript checks the following options of the command head
#  1) Option -n

test="head"

mkdir test_dir
echo "This is the 1st line." > test_dir/test1
echo "This is the 2nd line." >> test_dir/test1
echo "This is the 3rd line." >> test_dir/test1
busybox head -n 2 test_dir/test1 > log

if [ "$(busybox head -n 1 log)" = "This is the 1st line." ] && [ "$(tail -n 1 log)" = "This is the 2nd line." ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm log
rm -rf test_dir
