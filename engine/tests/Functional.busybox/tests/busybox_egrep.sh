#!/bin/sh

#  The testscript checks the following options of the command egrep
#  1) Option: -i

test="egrep"

echo "test file" >test1
echo "extended grep" >>test1
echo "Extended Grep" >>test1
busybox egrep 'test|extended' ./test1 > log1
if [ "$(head -n 1 log1)" = "test file" ] && [ "$(tail -n 1 log1)" = "extended grep" ]
then
    echo " -> $test: Egrep output verification#1 succeeded."
else
    echo " -> $test: Egrep output verification#1 failed."
    echo " -> $test: TEST-FAIL"
    rm log1
    rm -rf test1
    exit
fi;

busybox egrep -i 'test|extended' ./test1 > log2
if [ "$(head -n 1 log2)" = "test file" ] && [ "$(head -n 2 log2 | tail -n 1)" = "extended grep" ] &&  [ "$(tail -n 1 log2)" = "Extended Grep" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm log1 log2;
rm -rf test1;
