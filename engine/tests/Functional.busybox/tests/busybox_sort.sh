#!/bin/sh

#  The testscript checks the following options of the command sort
#  1) Option none

test="sort"

echo "e" > test1
echo "f" >> test1
echo "b" >> test1
echo "d" >> test1
echo "c" >> test1
echo "a" >> test1

busybox sort test1 > log
if [ "$(head -n 1 log)" = "a" ] && [ "$(head -n 2 log | tail -n 1)" = "b" ] && [ "$(head -n 3 log | tail -n 1)" = "c" ] \
&& [ "$(head -n 4 log | tail -n 1)" = "d" ] && [ "$(head -n 5 log | tail -n 1)" = "e" ] && [ "$(tail -n 1 log)" = "f" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm -rf test1;
rm log;
