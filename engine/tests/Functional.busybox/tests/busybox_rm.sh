#!/bin/sh

#  The testscript checks the following options of the command rm
#  1) Option: -r -f

test="rm"

mkdir -p test_dir/test_dir1
touch test_dir/test1
ls -l test_dir > log
if head -n 2 log | head -n 2 | grep "\-rw.*test1" && tail -n 1 log | grep "d.*test_dir1"
then
    echo " -> $test: mkdir test_dir verification succeeded."
else
    echo " -> $test: mkdir test_dir verification failed."
    echo " -> $test: TEST-FAIL"
    rm -rf test_dir
    rm log
    exit
fi;

busybox rm -f test_dir/test1
if ls -l test_dir | grep "d.*test_dir1"
then
    echo " -> &test: rm verification#2 succeeded."
else
    echo " -> &test: rm verification#2 failed."
    echo " -> &test: TEST-FAIL"
    rm -rf test_dir
    rm log
    exit
fi;

busybox rm -rf test_dir
if [ "$(ls -l | grep test_dir)" = "" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
rm log
