#!/bin/sh

#  The testscript checks the following options of the command echo
#  1) Option none

test="echo"

if [ "$(busybox echo "hello world")" = "hello world" ]
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi;
