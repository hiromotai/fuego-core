function test_pre_check {
    is_on_target_path named-checkconf PROGRAM_CHECKCONF
    assert_define PROGRAM_CHECKCONF "Missing 'bind' program on target board"
    is_on_target_path dig PROGRAM_DIG
    assert_define PROGRAM_DIG "Missing 'dig' program on target board"
    is_on_target_path named PROGRAM_NAMED
    assert_define PROGRAM_NAMED "Missing 'named' program on target board"
    is_on_target_path netstat PROGRAM_NETSTAT
    assert_define PROGRAM_NETSTAT "Missing 'netstat' program on target board"
    is_on_target_path rndc-confgen PROGRAM_CONFGEN
    assert_define PROGRAM_CONFGEN "Missing 'rndc-confgen' program on target board"
    is_on_target_path named-checkzone PROGRAM_CHECKZONE
    assert_define PROGRAM_CHECKZONE "Missing 'named-checkzone' program on target board"
}

function test_deploy {
    put $TEST_HOME/bind_test.sh $BOARD_TESTDIR/fuego.$TESTDIR/
    put $FUEGO_CORE/engine/scripts/fuego_board_function_lib.sh $BOARD_TESTDIR/fuego.$TESTDIR
    put -r $TEST_HOME/tests $BOARD_TESTDIR/fuego.$TESTDIR/
    put -r $TEST_HOME/data $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
    report "cd $BOARD_TESTDIR/fuego.$TESTDIR;\
    export tst_bind_file=246.168.192.in-addr.arpa.db;\
    export test_target_conf=x86_64;\
    export remotehost=$IPADDR;\
    ./bind_test.sh"
}

function test_processing {
    log_compare "$TESTDIR" "0" "TEST-FAIL" "n"
}
