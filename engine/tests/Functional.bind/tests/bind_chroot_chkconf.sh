#!/bin/sh

#  After running the/etc/named.conf to the target, run the named checkconf command in the chroot environment to verify the normal termination.

test="chroot_chkconf"

kill -9 $(pgrep named)

if [ ! -d /var/named/chroot/etc/bind ]
then
    mkdir -p /var/named/chroot/etc/bind
fi

if [ ! $(ls /var/named/chroot/etc/bind/) ]
then
    cp -f /etc/bind/* /var/named/chroot/etc/bind/
fi

if [ ! -d /var/named/chroot/var/named ]
then
    mkdir -p /var/named/chroot/var/named
fi

if [ ! -d /var/named/chroot/var/cache/bind ]
then
    mkdir -p /var/named/chroot/var/cache/bind
fi

if [ ! -d /var/named/chroot/var/run/named ]
then
    mkdir -p /var/named/chroot/var/run/named
fi

cp /etc/sysconfig/named /etc/sysconfig/named_bak
cp data/bind9/sysconfig/named /etc/sysconfig/named

if [ ! -f /var/named/chroot/etc/bind/named.conf ]
then
    touch /var/named/chroot/etc/bind/named.conf
fi

cp data/bind9/named.conf /var/named/chroot/etc/bind/named.conf

if named-checkconf -t /var/named/chroot
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

rm -rf /var/named
mv /etc/sysconfig/named_bak /etc/sysconfig/named
