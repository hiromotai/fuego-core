#!/bin/sh

#  Launch named chroot with the target and check if syslogis exist.

test="chroot_syslog-ng"

kill -9 $(pgrep named)

. ./fuego_board_function_lib.sh

set_init_manager

exec_service_on_target syslog-ng stop

if [ -f /var/log/syslog ]
then
    mv /var/log/syslog /var/log/syslog_bak
fi

if [ ! -d /var/named/chroot/etc/bind ]
then
    mkdir -p /var/named/chroot/etc/bind
fi

if [ ! $(ls /var/named/chroot/etc/bind/) ]
then
    cp -f /etc/bind/* /var/named/chroot/etc/bind/
fi

if [ ! -d /var/named/chroot/var/named ]
then
    mkdir -p /var/named/chroot/var/named
fi

if [ ! -d /var/named/chroot/var/cache/bind ]
then
    mkdir -p /var/named/chroot/var/cache/bind
fi

if [ ! -d /var/named/chroot/var/run/named ]
then
    mkdir -p /var/named/chroot/var/run/named
fi

exec_service_on_target syslog-ng restart

named -t /var/named/chroot

if cat /var/log/syslog | grep "starting up"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
kill -9 $(pgrep named)
rm -rf /var/named
