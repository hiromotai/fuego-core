#!/bin/sh

#  In the target execute command rndc-confgen, and confirm the file rndc.key.

test="rndc-confgen"

. ./fuego_board_function_lib.sh

set_init_manager

exec_service_on_target named stop

if [ -f /etc/bind/rndc.key ]
then
    mv /etc/bind/rndc.key /etc/bind/rndc.key_bak
fi

rndc-confgen -a -k rndckey -r /dev/urandom

echo "sleep 60 seconds"
sleep 60

if ls /etc/bind/rndc.key
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

if [ -f /etc/bind/rndc.key_bak ]
then
    mv /etc/bind/rndc.key_bak /etc/bind/rndc.key
else
    rm -f /etc/bind/rndc.key
fi
