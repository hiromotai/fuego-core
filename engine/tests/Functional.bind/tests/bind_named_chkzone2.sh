#!/bin/sh

# when the /var/named/ is transformed run the named checkzone command to verify that it is successfully terminated.

test="named_chkzone2"

. ./fuego_board_function_lib.sh

set_init_manager

exec_service_on_target named stop

if [ ! -d /var/named ]
then
    mkdir -p /var/named
fi

cp data/bind9/$tst_bind_file /var/named/$tst_bind_file
cp data/bind9/linux_test.com.db_$test_target_conf /var/named/linux_test.com.db

if named-checkzone 192.168.0.0 /var/named/$tst_bind_file
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

rm -f /var/named/$tst_bind_file /var/named/linux_test.com.db
