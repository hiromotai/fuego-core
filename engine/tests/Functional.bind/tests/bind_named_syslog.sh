#!/bin/sh

# Start the named on target.
# Check the log of named.

test="named_syslog"

. ./fuego_board_function_lib.sh

set_init_manager
logger_service=$(detect_logger_service)

if [ -f /etc/bind/rndc.key ]
then
    rm -f /etc/bind/rndc.key
fi

exec_service_on_target named stop
exec_service_on_target syslog.socket stop
exec_service_on_target $logger_service stop

if [ -f /var/log/syslog ]
then
    mv /var/log/syslog /var/log/syslog_bak
fi

exec_service_on_target $logger_service restart
sleep 5

if exec_service_on_target named start
then
    echo " -> start of named succeeded."
else
    echo " -> start of named failed."
    echo " -> $test: TEST-FAIL"
    exec_service_on_target named stop
    if [ -f /var/log/syslog_bak ]
    then
        mv /var/log/syslog_bak /var/log/syslog
    fi
    exit
fi

if cat /var/log/syslog | grep "starting up"
then
    echo " -> $test: TEST-PASS"
else
    echo " -> log is not generated."
    echo " -> $test: TEST-FAIL"
fi

exec_service_on_target named stop
if [ -f /var/log/syslog_bak ]
then
    mv /var/log/syslog_bak /var/log/syslog
fi

