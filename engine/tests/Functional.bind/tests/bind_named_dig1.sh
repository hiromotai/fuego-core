#!/bin/sh

# Start the named on target.
# Check the IP address.

test="named_dig1"

. ./fuego_board_function_lib.sh

set_init_manager

exec_service_on_target named stop

if [ ! -f /etc/bind/named.conf ]
then
    touch /etc/bind/named.conf
fi

if [ ! -f /etc/bind/rndc.conf ]
then
    touch /etc/bind/rndc.conf
fi

if [ ! -f /etc/resolv.conf ]
then
    touch /etc/resolv.conf
fi

mv /etc/resolv.conf /etc/resolv.conf_bak
cp data/bind9/resolv.conf /etc/resolv.conf
cp data/bind9/sysconfig/named.nochroot /etc/sysconfig/named

mv /etc/bind/named.conf /etc/bind/named.conf_bak
cp data/bind9/named.conf /etc/bind/named.conf

mv /etc/bind/rndc.conf /etc/bind/rndc.conf_bak
cp data/bind9/rndc.conf /etc/bind/rndc.conf

if [ ! -d /var/named ]
then
    mkdir -p /var/named
fi

cp data/bind9/$tst_bind_file /var/named/$tst_bind_file
cp data/bind9/linux_test.com.db_$test_target_conf /var/named/linux_test.com.db
if [ ! -f /etc/hosts ]
then
    touch /etc/hosts
fi
mv /etc/hosts /etc/hosts_bak
cp data/bind9/hosts /etc/hosts

exec_service_on_target named start

if dig linux-test.com | grep $remotehost
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi

exec_service_on_target named stop

mv /etc/resolv.conf_bak /etc/resolv.conf
mv /etc/bind/named.conf_bak /etc/bind/named.conf
mv /etc/bind/rndc.conf_bak /etc/bind/rndc.conf
rm -fr /var/named
mv /etc/hosts_bak /etc/hosts
