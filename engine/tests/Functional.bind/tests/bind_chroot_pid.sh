#!/bin/sh

#  In the target start named chroot, and confirm the process file id.

test="chroot_pid"

killall named

if [ ! -d /var/named/chroot/etc/bind ]
then
    mkdir -p /var/named/chroot/etc/bind
fi

if [ ! $(ls /var/named/chroot/etc/bind/) ]
then
    cp -f /etc/bind/* /var/named/chroot/etc/bind/
fi

if [ ! -d /var/named/chroot/var/named ]
then
    mkdir -p /var/named/chroot/var/named
fi

if [ ! -d /var/named/chroot/var/cache/bind ]
then
    mkdir -p /var/named/chroot/var/cache/bind
fi

if [ ! -d /var/named/chroot/var/run/named ]
then
    mkdir -p /var/named/chroot/var/run/named
fi

if [ -f /var/named/chroot/var/run/named/named.pid ]
then
    rm -f /var/named/chroot/var/run/named/named.pid
fi

cp /etc/sysconfig/named /etc/sysconfig/named_bak
cp data/bind9/sysconfig/named /etc/sysconfig/named

if [ ! -f /etc/resolv.conf ]
then
    touch /etc/resolv.conf
fi

cp /etc/resolv.conf /etc/resolv.conf_bak
cp data/bind9/resolv.conf /etc/resolv.conf

if [ ! -f /var/named/chroot/etc/bind/named.conf ]
then
    touch /var/named/chroot/etc/bind/named.conf
fi

cp /var/named/chroot/etc/bind/named.conf /var/named/chroot/etc/bind/named.conf_bak
cp data/bind9/named.conf /var/named/chroot/etc/bind/named.conf

if [ ! -f /etc/bind/named.conf ]
then
    touch /etc/bind/named.conf
fi
mv /etc/bind/named.conf /etc/bind/named.conf_bak
ln -s /var/named/chroot/etc/bind/named.conf /etc/bind/named.conf

if [ ! -f /var/named/chroot/etc/bind/rndc.conf ]
then
    touch /var/named/chroot/etc/bind/rndc.conf
fi
cp /var/named/chroot/etc/bind/rndc.conf /var/named/chroot/etc/bind/rndc.conf_bak
cp data/bind9/rndc.conf /var/named/chroot/etc/bind/rndc.conf

if [ ! -f /etc/bind/rndc.conf ]
then
    touch /etc/bind/rndc.conf
fi
mv /etc/bind/rndc.conf /etc/bind/rndc.conf_bak
ln -s /var/named/chroot/etc/bind/rndc.conf /etc/bind/rndc.conf

if [ ! -f /var/named/chroot/etc/bind/rndc.key ]
then
    touch /var/named/chroot/etc/bind/rndc.key
fi
cp /var/named/chroot/etc/bind/rndc.key /var/named/chroot/etc/bind/rndc.key_bak
cp data/bind9/rndc.key /var/named/chroot/etc/bind/rndc.key

if [ ! -f /etc/bind/rndc.key ]
then
    touch /etc/bind/rndc.key
fi
mv /etc/bind/rndc.key /etc/bind/rndc.key_bak
ln -s /var/named/chroot/etc/bind/rndc.key /etc/bind/rndc.key

cp data/bind9/$tst_bind_file /var/named/chroot/var/named/$tst_bind_file
cp data/bind9/linux_test.com.db_$test_target_conf /var/named/chroot/var/named/linux_test.com.db
if [ ! -f /etc/hosts ]
then
    touch /etc/hosts
fi
mv /etc/hosts /etc/hosts_bak
cp data/bind9/hosts /etc/hosts

restore_target() {
    rm -rf /var/named
    mv /etc/sysconfig/named_bak /etc/sysconfig/named
    mv /etc/resolv.conf_bak /etc/resolv.conf
    mv /etc/hosts_bak /etc/hosts
    mv /etc/bind/named.conf_bak /etc/bind/named.conf
    mv /etc/bind/rndc.conf_bak /etc/bind/rndc.conf
    mv /etc/bind/rndc.key_bak /etc/bind/rndc.key
    chown root.named /etc/bind/rndc.key /etc/bind/rndc.conf  /etc/bind/named.conf
    chmod 644 /etc/bind/rndc.key /etc/bind/rndc.conf  /etc/bind/named.conf
}

named -t /var/named/chroot

if ls /var/named/chroot/var/run/named/named.pid
then
    echo " -> pid file is exist."
else
    echo " -> pid file is not exist."
    echo " -> $test: TEST-FAIL"
    killall named
    restore_target
    exit
fi

killall named

sleep 5

if test -f /var/named/chroot/var/run/named/named.pid
then
    echo " -> $test: TEST-FAIL"
else
    echo " -> $test: TEST-PASS"
fi
restore_target
