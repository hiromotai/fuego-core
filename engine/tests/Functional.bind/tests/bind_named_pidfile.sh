#!/bin/sh

# Start the named on target.
# Check the pid file.

test="named_pidfile"

. ./fuego_board_function_lib.sh

set_init_manager

if [ -f /etc/bind/rndc.key ]
then
    rm -f /etc/bind/rndc.key
fi

exec_service_on_target named stop

if exec_service_on_target named start
then
    echo " -> start of named succeeded."
else
    echo " -> start of named failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if ls /var/run/named/named.pid
then
    echo " -> pid file of named is exist."
else
    echo " -> pid file of named is not exist."
    echo " -> $test: TEST-FAIL"
    exec_service_on_target named stop
    exit
fi

if exec_service_on_target named stop
then
    echo " -> stop of named succeeded."
else
    echo " -> stop of named failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if test ! -f /var/run/named/named.pid
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
