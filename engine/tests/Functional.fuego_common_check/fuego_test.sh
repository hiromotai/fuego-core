
function test_run {
    report "echo Starting common_check"

    # now do some host-driven tests
    tmpfile=$(mktemp)

    # put tests here
    test_num=1
    empty="\"\""
    # test line format is: "<function> <arg1> <arg2> <arg3> <expected result>"
    for test_str in "startswith abcdef abc - r0" \
      "startswith abcdef def - r1" \
      "startswith abc abcdef - r1" \
      "endswith abcdef def - r0" \
      "endswith abcdef abc - r1" \
      "endswith abc abcdef - r1" \
      "endswith abc=y =y - r0" \
      "endswith abd=n =y - r1" \
      "slice abcdef 0 3 abc" \
      "slice abcdef 3 6 def" \
      "slice abcdef 3 10 def" \
      "slice abcdef 0 -3 abc" \
      "slice abcdef -3 5 de" \
      "slice abcdef -10 -1 abcde" \
      ; do

        args=($test_str)
        func_name=${args[0]}
        arg1=${args[1]}
        arg2=${args[2]}
        arg3=${args[3]}
        expected=${args[4]}

        # convert to empty strings
        if [ "$arg1" == "$empty" ] ; then
          arg1=""
        fi
        if [ "$arg2" == "$empty" ] ; then
          arg2=""
        fi

        # perform function call, with requested args
        set +e
        if [ "$arg3" == "-" ] ; then
          result=$($func_name $arg1 $arg2)
          rcode="$?"
        else
          result=$($func_name $arg1 $arg2 $arg3)
          rcode="$?"
        fi
        set -e

        # if table output starts with 'r', then check return code
        # instead of function output
        if [ "${expected%%[01]}" == "r" ] ; then
          # test return code
          result="r$rcode"
        fi

        if [ "$result" == "$expected" ] ; then
          echo -n "ok" >>$tmpfile
        else
          echo -n "not ok" >>$tmpfile
        fi

        # output result in TAP format
        echo -n " $test_num - " >>$tmpfile
        test_num=$(( $test_num + 1))
        echo -n $test_str >>$tmpfile
        echo " : result=$result" >>$tmpfile
    done
    test_num=$(( $test_num - 1 ))
    echo "1..$test_num"

    # put the host log into the target log
    put $tmpfile $tmpfile
    report_append "cat $tmpfile"
    rm "$tmpfile"

    report_append "echo Test done"
}

function test_processing {
    log_compare "$TESTDIR" "$test_num" "^ok" "p"
}
