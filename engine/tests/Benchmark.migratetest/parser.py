#!/usr/bin/python
import os, re, sys
sys.path.insert(0, os.environ['FUEGO_CORE'] + '/engine/scripts/parser')
import common as plib

regex_string_max = "Max:\s+(\d+)\s+us"
regex_string_min = "Min:\s+(\d+)\s+us"
regex_string_avg = "Avg:\s+(\d+)\s+us"
measurements = {}
matches_max = plib.parse_log(regex_string_max)
matches_min = plib.parse_log(regex_string_min)
matches_avg = plib.parse_log(regex_string_avg)

if matches_max and matches_min and matches_avg:
	min_intervals = []
	avg_intervals = []
	max_intervals = []
	for thread_max in matches_max:
		max_intervals.append(float(thread_max))
	for thread_min in matches_min:
		min_intervals.append(float(thread_min))
	for thread_avg in matches_avg:
		avg_intervals.append(float(thread_avg))
	measurements['default.intervals'] = [
		{"name": "max_interval", "measure" : max(max_intervals)},
		{"name": "min_interval", "measure" : min(min_intervals)},
		{"name": "avg_interval", "measure" : sum(avg_intervals)/len(avg_intervals)}]

sys.exit(plib.process(measurements))
