tarball=osdl-aim-7.0.1.13.tar.gz

function test_build {
        ./bootstrap
        PKG_CONFIG_PATH=${SDKROOT}/usr/lib/pkgconfig PKG_CONFIG_ALLOW_SYSTEM_LIBS=1 PKG_CONFIG_SYSROOT_DIR=${SDKROOT} ./configure --host=$HOST --build=`uname -m`-linux-gnu LDFLAGS=-L${SDKROOT}/usr/lib CPPFLAGS=-I${SDKROOT}/usr/include  CFLAGS=-I${SDKROOT}/usr/include LIBS=-laio --prefix=$BOARD_TESTDIR/$TESTDIR --datarootdir=$BOARD_TESTDIR/$TESTDIR
        make
}

function test_deploy {
	put src/reaim  $BOARD_TESTDIR/fuego.$TESTDIR/
	put data  $BOARD_TESTDIR/fuego.$TESTDIR/
	put scripts  $BOARD_TESTDIR/fuego.$TESTDIR/
}

function test_run {
	report "cd $BOARD_TESTDIR/fuego.$TESTDIR; mkdir /tmp/diskdir; ./reaim -c ./data/reaim.config -f ./data/workfile.short"
	report_append "cd $BOARD_TESTDIR/fuego.$TESTDIR; ./reaim -c ./data/reaim.config -f ./data/workfile.all_utime"
}


