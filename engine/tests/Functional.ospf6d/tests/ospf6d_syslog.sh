#!/bin/sh

#  In the target start ospf6d and zebra.
#  At the same time, start syslog-ng and check the keyword "ospf6d" in syslog.

test="syslog"

. ./fuego_board_function_lib.sh

set_init_manager

exec_service_on_target ospf6d stop
exec_service_on_target zebra stop
exec_service_on_target syslog-ng stop

rm -f /var/run/quagga/ospf6d.pid
#Backup the config file
mv /etc/quagga/ospf6d.conf /etc/quagga/ospf6d.conf.bck
mv /etc/quagga/zebra.conf /etc/quagga/zebra.conf.bck
#Backup the config file
mv /var/log/syslog /var/log/syslog.bck

cp data/ospf6d.conf /etc/quagga/ospf6d.conf
cp data/zebra.conf /etc/quagga/zebra.conf
chown quagga:quagga /etc/quagga/*.conf

if exec_service_on_target syslog-ng restart
then
    echo " -> restart of syslog-ng succeeded."
else
    echo " -> restart of syslog-ng failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if exec_service_on_target zebra start
then
    echo " -> start of zebra succeeded."
else
    echo " -> start of zebra failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if exec_service_on_target ospf6d start
then
    echo " -> start of ospf6d succeeded."
else
    echo " -> start of ospf6d failed."
    echo " -> $test: TEST-FAIL"
    exit
fi

if cat /var/log/syslog | grep "ospf6d"
then
    echo " -> get the syslog of ospf6d."
    echo " -> $test: TEST-PASS"
else
    echo " -> can't get the syslog of ospf6d."
    echo " -> $test: TEST-FAIL"
    exit
fi

exec_service_on_target ospf6d stop
exec_service_on_target zebra stop

#Restore the config file
mv /etc/quagga/ospf6d.conf.bck /etc/quagga/ospf6d.conf
mv /etc/quagga/zebra.conf.bck /etc/quagga/zebra.conf
#Restore the config file
mv /var/log/syslog.bck /var/log/syslog
