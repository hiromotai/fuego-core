#!/usr/bin/python
# See common.py for description of command-line arguments

import os
import sys
import collections
import re

sys.path.insert(0, os.environ['FUEGO_CORE'] + '/engine/scripts/parser')
import common as plib

results = collections.OrderedDict()

# LTP results come in largely 2 formats:
# sched_getattr01    1  TPASS  :  attributes were read back correctly
# request_key01.c:49: PASS: request_key() succeed

log_lines = open(plib.TEST_LOG, "r").readlines()
re_pattern = r"(\S+)\s+(\S)+\s+(TPASS|TCONF)\s+:(.*)"

testcase_count = 1
for line in log_lines:
    result = None
    #print "line=", line
    if ".c:" in line:
        parts = line.split(":")
        filename = parts[0]
        line_no = parts[1]
        result = parts[2].strip()
        message = parts[3].strip()
        if result == "INFO":
            result = None
        else:
            testcase_num = str(testcase_count)
            testcase_count += 1
    else:
        m = re.match(re_pattern, line)
        #print "m=", m
        if m:
            test_name = m.group(1)
            testcase_num = m.group(2)
            result = m.group(3)[1:] # strip off the leading 'T'
            message = m.group(4).strip()

    # translate to Fuego results
    if result:
        if result == 'PASS':
            results[testcase_num] = 'PASS'
        elif result == 'FAIL':
            results[testcase_num] = 'FAIL'
        elif result == 'CONF':
            results[testcase_num] = 'SKIP'
        else:
            results[testcase_num] = 'ERR'

sys.exit(plib.process(results))
