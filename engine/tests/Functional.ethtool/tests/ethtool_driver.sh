#!/bin/sh

#  In target, run command ethtool.
#  option: -i

test="driver"

ETHERNET_DEVICE_NAME="have no Ethernet device"
ifconfig | cut -d' ' -f1 | sed '/^$/d' > driver_list

for line in $(cat driver_list)
do
    if ethtool $line | grep "baseT"
    then
        ETHERNET_DEVICE_NAME=$line
        break
    fi
done

if ethtool -i $ETHERNET_DEVICE_NAME | grep driver
then
    echo " -> $test: TEST-PASS"
else
    echo " -> $test: TEST-FAIL"
fi
